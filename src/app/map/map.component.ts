import { Component, OnInit } from '@angular/core';

import { MapService } from '../services/map.service';
import { IActivity } from '../shared/activity.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  // inject mapsService into the map component and also injecs the activated route
  constructor(private _mapService: MapService,
              private _route: ActivatedRoute) { }

  //properties of the class
  activity: any;
  activityName: string;
  activityComments: string;
  activityDate: Date;
  activityDistance: number;
  gpx: any;

  ngOnInit() {
    // get the activity id from the url route run/:id and get the correspondent activity
    this.activity = this._mapService.getActivity(
      +this._route.snapshot.params['id']
    )
  }

  ngAfterViewInit(){
    // plot the map
    this._mapService.plotActivity(+this._route.snapshot.params['id']);
    this.activityName = this.activity.name;
    this.activityComments = this.activity.comments;
    this.activityDistance = this.activity.distance;
    this.activityDate = this.activity.date;
    this.gpx = this.activity.gpxData;
  }
}
