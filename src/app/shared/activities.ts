import { IActivity } from './activity.model';

export const SAVED_ACTIVITIES: IActivity[] =[
  {
    "id": 1,
    "name": "main bike trails",
    "date": new Date('8/30/2017'),
    "distance": 7.2,
    "comments": "Nice day",
    "gpxData": '../../assets/gpx/jafra.gpx'
  },
  {
    "id": 2,
    "name": "per barcelona en bicicleta",
    "date": new Date('3/06/2012'),
    "distance": 38.2,
    "comments": "Nice day on bike",
    "gpxData": '../../assets/gpx/per-barcelona-en-bicicleta.gpx'
  },
  {
    "id": 3,
    "name": "ruta paseo maritimo barcelona",
    "date": new Date('07/02/2013'),
    "distance": 10.3,
    "comments": "Nice day",
    "gpxData": '../../assets/gpx/ruta-paseo-maritimo-barcelona-garmin.gpx'
  }
]
