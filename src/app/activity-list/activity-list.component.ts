import { Component, OnInit } from '@angular/core';
import { IActivity } from '../shared/activity.model';
import { ActivityService} from '../services/activity.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {

  // create some properties
  activities: IActivity[];
  totalActivities: number
  totalDistance: number
  firstDate: Date

  // inject the activite service into the activity component
  constructor(private _activityService: ActivityService) { }

  // on init we'll get instances from this properties with this methods
  ngOnInit() {
    this.activities = this._activityService.getActivities();// get all activities
    this.totalActivities = this._activityService.getTotalActivities(this.activities);
    this.totalDistance = this._activityService.getTotalDistance(this.activities);
    this.firstDate = this._activityService.getFirstDate(this.activities);
  }

}
